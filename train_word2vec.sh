cd src

TF_CFLAGS=( $(python -c 'import tensorflow as tf; print(" ".join(tf.sysconfig.get_compile_flags()))') )
TF_LFLAGS=( $(python -c 'import tensorflow as tf; print(" ".join(tf.sysconfig.get_link_flags()))') )
g++ -std=c++11 -shared word2vec_ops.cc word2vec_kernels.cc -o word2vec_ops.so -fPIC ${TF_CFLAGS[@]} ${TF_LFLAGS[@]} -O2 -D_GLIBCXX_USE_CXX11_ABI=0

cd ..

### Download and preprocess wiki corpus if specified in docker-compose.yml
if [ "$DOWNLOAD_WIKI" == "true"  ]
then
    echo "Downloading Wikipedia dump: $WIKI_PREFIX$WIKI_DUMP"
    wget $WIKI_PREFIX$WIKI_DUMP
fi

if [ "$CORPUS_TYPE" == "wiki" ]
then
    echo "Preprocessing Wikipedia Dump"
    python process_wikipedia.py $WIKI_DUMP $CORPUS
fi

python src/word2vec.py \
  --train_data=$CORPUS \
  --eval_data=$EVAL_FILE \
  --save_path=$OUTPUTDIR \
  --embedding_size=$VECTOR_SIZE \
  --epochs_to_train=$MAX_ITER \
  --learning_rate=$LEARNING_RATE \
  --batch_size=$BATCH_SIZE \
  --window_size=$WINDOW_SIZE \
  --min_count=$VOCAB_MIN_COUNT

## write model meta file
VOCAB_SIZE=$(cat $OUTPUTDIR/vocab.txt | wc -l)
DATE=$(date +%Y-%m-%d)
echo "{
  \"alias\": \"word2vec\",
  \"embeddings_filename\": \"vectors.txt\",
  \"vocab_filename\": \"vocab.txt\",
  \"input_field\": \"$TEXT_TYPE\",
  \"token_spacer\": \"$TOKEN_SPACER\",
  \"vocab_size\": $VOCAB_SIZE,
  \"train_params\": {
    \"VOCAB_MIN_COUNT\": $VOCAB_MIN_COUNT,
    \"VECTOR_SIZE\": $VECTOR_SIZE,
    \"MAX_ITER\": $MAX_ITER,
    \"WINDOW_SIZE\": $WINDOW_SIZE,
    \"LEARNING_RATE\": $LEARNING_RATE,
    \"BATCH_SIZE\": $BATCH_SIZE
  },
  \"train_date\": \"$DATE\",
  \"notes\": \"$META_NOTES\"
}" > $OUTPUTDIR/word_emb.meta